@file:Suppress("unused")

package com.xerini.interview.server.api

import java.util.concurrent.CopyOnWriteArrayList
import javax.ws.rs.*
import javax.ws.rs.core.MediaType.APPLICATION_JSON
import javax.ws.rs.core.Response

@Path("/geo-json")
class GeoJsonService {
    private val allCoordinates = CopyOnWriteArrayList<HashMap<List<Double>, String>>()

    @GET
    @Produces(APPLICATION_JSON)
    fun getGeoJson(): GeoJsonObject {
        val features: MutableList<GeoJsonFeature> = mutableListOf()
        allCoordinates.forEach {
            for ((key, value) in it) {
                val properties = mutableMapOf<String, Any?>("metaData" to value)
                features += GeoJsonFeature(GeometryData(key), properties)
            }
        }

        return GeoJsonObject(features)
    }

    @Path("/add")
    @POST
    @Consumes(APPLICATION_JSON)
    fun addPoint(requestBody: RequestBody): Response {
        return try {
            val hashMap: HashMap<List<Double>, String> = hashMapOf()
            hashMap[requestBody.coordinates] = requestBody.metaData
            allCoordinates.add(hashMap)
            val properties = mutableMapOf<String, Any?>("metaData" to requestBody.metaData)
            Response.ok(GeoJsonFeature(GeometryData(requestBody.coordinates), properties)).build();
        } catch (e: Exception) {
            Response.serverError().entity(e).build();
        }

    }
}

data class RequestBody(val coordinates: List<Double>, val metaData: String) {
    val type: String = "MetaDataCollection"
}

data class GeoJsonObject(val features: List<GeoJsonFeature>) {
    val type: String = "FeatureCollection"
}

data class GeoJsonFeature(val geometry: GeometryData?, val properties: Map<String, Any?> = emptyMap()) {
    val type: String = "Feature"
}

data class GeometryData(val coordinates: List<Double>) {
    val type: String = "Point"
}