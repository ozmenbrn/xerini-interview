import * as React from "react";
import { useEffect, useState } from "react";
import { Feature, Map, MapBrowserEvent, View } from "ol";
import { fromLonLat, toLonLat } from "ol/proj";
import TileLayer from "ol/layer/Tile";
import { OSM, Vector, Cluster } from "ol/source";
import "./map-view.css";
import Overlay from "ol/Overlay";

import "ol/ol.css";
import { FeatureLike } from "ol/Feature";
import VectorLayer from "ol/layer/Vector";
import { GeoJSON } from "ol/format";

import {
  Icon,
  Style,
  Circle as CircleStyle,
  Stroke,
  Fill,
  Text,
} from "ol/style";
import { Coordinate } from "ol/coordinate";
import { StyleLike } from "ol/style/Style";

const geoJson = new GeoJSON();

const mapPinStyle = new Style({
  image: new Icon({
    src: "/img/map-pin-blue.png",
    scale: 25 / 50,
    anchor: [0.5, 1.0],
  }),
});

export const MapView: React.FC = () => {
  const [map, setMap] = useState<Map | undefined>(undefined);
  const [featureLayer, setFeatureLayer] = useState<VectorLayer | undefined>();
  const [features, setFeatures] = useState<FeatureLike[]>([]);
  const [newFeature, setNewFeature] = useState<FeatureLike>();
  const [clusterDistance, setClusterDistance] = useState(40);
  const [styleCache, setStyleCache] = useState<any>({});
  const [metaData, setMetaData] = useState("");
  const [givenCoordinate, setGivenCoordinate] = useState(null);

  useEffect(() => {
    const map = new Map({
      target: "map",
      layers: [
        new TileLayer({
          source: new OSM(),
        }),
      ],
      view: new View({
        center: fromLonLat([-0.023758, 51.547504]),
        zoom: 13,
        minZoom: 6,
        maxZoom: 18,
      }),
    });
    map.on("click", onMapClick);

    setMap(map);
    loadFeatureData();
  }, []);

  useEffect(() => {
    if (map) {
      setFeatureLayer(addFeatureLayer(featureLayer, features));
    }
  }, [map, features]);

  useEffect(() => {
    if (map && newFeature) {
      features.push(newFeature);
      setFeatureLayer(addFeatureLayer(featureLayer, features));
      setFeatures([...features]);
    }
  }, [newFeature]);

  const saveFeatureData = (coordinates: Coordinate) => {
    const requestOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ coordinates: coordinates, metaData: metaData }),
    };

    fetch(`/api/geo-json/add`, requestOptions)
      .then((response) => response.json())
      .then((json) => {
        setNewFeature(geoJson.readFeature(json));
      });
  };

  useEffect(() => {
    if (givenCoordinate) {
      saveFeatureData(givenCoordinate);
    }
  }, [givenCoordinate]);

  const getCircleStyles = (): StyleLike => {
    return function (feature: any) {
      const size = feature.get("features").length;
      let style = styleCache[size];
      if (!style) {
        style = new Style({
          image: new CircleStyle({
            radius: 10,
            stroke: new Stroke({
              color: "#fff",
            }),
            fill: new Fill({
              color: "#3399CC",
            }),
          }),
          text: new Text({
            text: size.toString(),
            fill: new Fill({
              color: "#fff",
            }),
          }),
        });
        styleCache[size] = style;
        setStyleCache(styleCache);
      }
      return style;
    };
  };

  const loadFeatureData = () => {
    fetch("/api/geo-json")
      .then((response) => response.json())
      .then((json) => setFeatures(geoJson.readFeatures(json)));
  };

  const addFeatureLayer = (
    previousLayer: VectorLayer,
    features: FeatureLike[]
  ): VectorLayer => {
    const clusters = previousLayer
      ? previousLayer
      : new VectorLayer({
          style: getCircleStyles(),
        });

    if (previousLayer != undefined) {
      previousLayer.setStyle(getCircleStyles());
      previousLayer.getSource().clear();
    } else {
      map.addLayer(clusters);
    }

    (clusters as any).tag = "features";

    const source = new Vector({
      format: geoJson,
      features: features as Feature<any>[],
    });

    const clusterSource = new Cluster({
      distance: parseInt(clusterDistance.toString(), 10),
      source: source,
    });

    clusters.setSource(clusterSource);

    return clusters;
  };

  const onMapClick = (e: MapBrowserEvent) => {
    setGivenCoordinate(e.coordinate);
  };

  const onClusterDistanceChange = (event: any) => {
    setClusterDistance(event.target.value);
    setFeatureLayer(addFeatureLayer(featureLayer, features));
  };

  const renderInputField = (
    id: string,
    value: number,
    onChangeFunction: Function
  ) => {
    return (
      <input
        className="input-class"
        id={id}
        type="range"
        min="0"
        max="200"
        step="1"
        value={value}
        onChange={(data: any) => onChangeFunction(data)}
      />
    );
  };

  const renderMetaData = () => {
    return features ? (
      <ul>
        {features.map((feature) => {
          return feature.getProperties() && feature.getProperties().metaData ? (
            <li>{`${toLonLat(
              feature.getProperties().geometry.flatCoordinates
            )} -> ${feature.getProperties().metaData}`}</li>
          ) : (
            <li>
              {toLonLat(feature.getProperties().geometry.flatCoordinates)}
            </li>
          );
        })}
      </ul>
    ) : (
      <div></div>
    );
  };

  return (
    <div>
      <div id="map" className="map" />

      <div className="row">
        <div className="col-sm-3">
          <label className="label-class">Marker Meta Data</label>
          <input
            id={"meta-data"}
            className="input-class"
            value={metaData}
            onChange={(data: any) => setMetaData(data.target.value)}
          />
        </div>
        <div className="col-sm-9">
          <label className="label-class">Cluster distance</label>
          {renderInputField(
            "distance",
            clusterDistance,
            onClusterDistanceChange
          )}
        </div>
      </div>
      <label className="mt-5">Meta Data</label>
      {renderMetaData()}
    </div>
  );
};
